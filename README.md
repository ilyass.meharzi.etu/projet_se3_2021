##  Projet de Programmation Avancée 

Ce dépôt `GIT` contient les livrables de MEHARZI ILyass et SIFA Omar pour le projet de Programmation Avancée.
Le but de ce tutorat est de réaliser une application codée en langage C, permettant à un utilisateur de réaliser des requêtes qui interrogent une base de données contenant les
informations de 58492 vols intranationaux qui ont eu lieu aux Etats-Unis en 2014.


## COMPILATION

La compilation de notre programme est automatisée grâce au Makefile. Le compilateur utilisé est `gcc` avec les options `-g -W -Wall -Wextra -MMD`.

La commande `make` initie les étapes de compilation suivantes:
le Makefile va générer les fichiers objet(.o) et l'exécutable à partir des fichier (.o) .

## CONTENU 

Ce dépôt contient à la racine différents répertoires :

-includes: fichiers header (.h)
     * inout.h: inclusion des librairies standard + définition des constantes + définition des structures de données+ prototypes des procédures de hachage+prototypes des procédures de lecture/affichage des données+prototypes des procédures des requêtes+prototypes des procédures d'interprétation des requêtes utilisateur

-data:
     * LICENSE: certification de libre accès aux données
     * airlines.csv: données des compagnies aériennes
     * airports.csv: données des aéroports
     * flights.csv: données des vols
     * requetes.txt: batterie de requêtes tests

-D'autres fichiers se trouvent à la racine:
     * Makefile: fichier établissant les règles de compilation
     * rapport.pdf: compte-rendu au format PDF
     * projet.c: contient tout les fonction
     
## UTILISATION DU PROGRAMME

Pour exécuter le programme il suffit d'entrer la commande "./project" .
Pour exécuter le programme avec redirection il suffit d'entrer la commande `./project < data/requetes.txt` .


Les requêtes renseignables sont les suivantes:

- `show-airports <airline_id>` : affiche tous les aéroports depuis lesquels la compagnie aérienne <airline_id> opère des vols
- `show-airlines <port_id>` : affiche les compagnies aériens qui ont des vols qui partent de l'aéroport <port_id>
- `show-flights <port_id> <date> [<time>] [limit=<xx>]` : affiche les vols qui partent de l'aéroport à la date, avec optionnellement une heure de début, et limité à xx vols
- `most-delayed-flights` : donne les 5 vols qui ont subis les plus longs retards à l'arrivée
- `most-delayed-airlines` : donne les 5 compagnies aériennes qui ont, en moyenne, le plus de retards
- `delayed-airline <airline_id>` : donne le retard moyen de la compagnie aérienne passée en paramètre
- `most-delayed-airlines-at-airport <airport_id>` : donne les 3 compagnies aériennes avec le plus de retard d'arrivé à l'aéroport passée en paramètre
- `changed-flights <date>` : les vols annulés ou déviés à la date  (format M-D)
- `avg-flight-duration <port_id> <port_id>` : calcule le temps de vol moyen entre deux aéroports
- `find-itinerary <port_id> <port_id> <date> [<time>] [limit=<xx>]` : trouve un ou plusieurs itinéraires entre deux aéroports à une date donnée (l'heure est optionnel, requête peut être limité à xx propositions, il peut y avoir des escales)
- `help` : affiche la liste des requêtes valides
- `quit` : quitte le programme

Pour information, les paramètres entre crochets `[ ]` sont optionnels et les paramètres entre `< >` indiquent une valeur à renseigner.
Les dates sont au format `M-J` et l'heure `HHMM`.

