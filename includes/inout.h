#include<stdio.h>
#include<stdlib.h>
#include<string.h>

FILE *fairlines;
FILE *fairports;
FILE *fflight;

FILE * fp;

#define maxCode 4
#define maxAIRP 3
#define maxAIRL 50
#define maxAirport 60
#define maxCity 50
#define SIZE 1500

#define DATE 1
#define AIRLINE 2
#define DEST_AIRPORT 3
#define DEP_AIRPORT 4

#define OUI 1
#define NON 0
#define bool signed char
struct flight {
  int month;
  int day;
  int weekday;
  char airline[maxAIRL];
  char org_air[maxCode];
  char dest_air[maxAIRP];
  int sched_dep;
  float dep_delay;
  float air_time;
  int dist;
  int sched_arr;
  int arr_delay;
  bool diverted;
  bool cancelled;
  
};

struct flightCellule {
  struct flight liste;
  struct flightCellule *suivant_date;
  struct flightCellule *suivant_airline;
  struct flightCellule *suivant_dest_airport;
  struct flightCellule *suivant_dep_airport;
};
typedef struct flightCellule * ptr_Hashtab_flight[SIZE];

/******************************definition of structure airlines******************************/
struct airlines {
  char iata_code[maxCode];
  char airlineN[maxAIRL];
  
};
struct airlinesCellule {
  struct airlines liste;
  struct airlinesCellule *suivant;
};

/******************************definition of structure airports******************************/
struct airports {
  char iata_code[maxCode];
  // struct IATACODE iata_code;
  char airport[maxAirport];//40
  char city[maxCity];
  char state[maxCode];
  char country[maxCity];
  float latitude;
  float longitude;

};

struct airportsCellule {
  struct airports liste;
  struct airportsCellule *suivant;
};
struct airline_delay
{
    struct airlines airline;
    int delay;
};


void ajout_tete_airline(struct airlinesCellule **, struct airlines );

int fichier_airlines(struct airlinesCellule **,FILE *);

void afficher_Airlines(struct airlinesCellule *);

void  ajout_tete_airports(struct airportsCellule **, struct  airports );

int  fichier_airports(struct airportsCellule **,FILE *);

void afficher_Airport (struct airportsCellule *);

void  init_table_hash( ptr_Hashtab_flight );

int function_hash_date(int ,int );

int function_hash_airline (char iata_airline[]);

int function_hash_dest_and_dep (char iata_airport[]);

int nb_flights (struct flightCellule * ,int );

void flight_init_cell(struct flightCellule **);

void adding_head_flight_read (struct flightCellule **,struct flightCellule **,struct flightCellule **,struct flightCellule **,struct flight );

int fichier_flights ( ptr_Hashtab_flight , ptr_Hashtab_flight , ptr_Hashtab_flight , ptr_Hashtab_flight , FILE  *);

void print_flights (struct flightCellule *,int );
void ajout_tete_flights(struct flightCellule ** ,struct flight  ,int );


void supp_flights_list(struct flightCellule ** ,int );

void supp_airlines_list(struct airlinesCellule **);

  
void supp_airports_list(struct airportsCellule ** );

void supp_data( ptr_Hashtab_flight , ptr_Hashtab_flight , ptr_Hashtab_flight , ptr_Hashtab_flight ,struct airlinesCellule** ,struct airportsCellule** );

int ajoute_airports_sansduplication(struct airportsCellule **,struct airports );

void ajoute_ordonner (struct flightCellule **, struct flight );

void show_airports(char iata_airline[] , ptr_Hashtab_flight ,struct airportsCellule * ,int );

int ajoute_airline_sansduplication (struct airlinesCellule **, struct airlines );


void show_airlines (char dep1[] , ptr_Hashtab_flight , struct airlinesCellule *, int );

void show_flights (char dep1[] , int , int , ptr_Hashtab_flight , int , int );

void most_delayed_flight( ptr_Hashtab_flight );

int delayed_airline (char  iata_airline[] , ptr_Hashtab_flight , struct airlinesCellule *, int );

void most_delayed_airlines (ptr_Hashtab_flight , struct airlinesCellule * , int );

void changed_flights (unsigned char month , unsigned char dayr ,  ptr_Hashtab_flight );

void  avg_flight_duration( char id_org[], char id_dest[]  , ptr_Hashtab_flight );



