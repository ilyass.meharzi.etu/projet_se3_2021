TARGET= project
CFLAGS=-g -W -Wall -Wextra -MMD
LDFLAGS=-lm
 
default: $(TARGET)
 
.c.o:
	gcc $(CFLAGS) -c $<
 
$(TARGET): projet.o inout.o 
	gcc $(LDFLAGS) $^ -o $@

-include $(DEPS)
 
.PHONY: clean
clean:
	rm -f *.o
	rm -f $(TARGET)
