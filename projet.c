/*ce fichier est fait par MEHARZI ILyass et SIFA Omar*/
#include<stdio.h>
#include<stdlib.h>
#include<string.h>

FILE *fairlines;
FILE *fairports;
FILE *fflight;

FILE * fp;

#include "includes/inout.h"

/******************************charger le fichier "airlines.csv"******************************/
void ajout_tete_airline(struct airlinesCellule **list_air, struct airlines cellule)
{
  struct airlinesCellule *tmp = malloc(sizeof(struct airlinesCellule));
  if (tmp == NULL) {
	  exit (EXIT_FAILURE);
  }    
      
	  tmp->liste =cellule;
          tmp->suivant=*list_air;
          *list_air =tmp;

}

int fichier_airlines(struct airlinesCellule **list_air,FILE *fairlines)
 { 
  if (fairlines ==NULL){
   exit (EXIT_FAILURE);
  }
    // char* token=strtok(list_air->liste->iata_code, ",");
  struct airlines cellule;
  int b=0;
  if(fscanf(fairlines,"%*[^\n]\n")!=0) exit (EXIT_FAILURE);
  
  while(fscanf(fairlines, "%2s,",cellule.iata_code) ==1 )
     {
       if(fscanf(fairlines,"%[A-Z a-z 0-9'.' ']s\n ",cellule.airlineN) != 1)exit (EXIT_FAILURE);
       /*  struct airlinesCellule *tmp = malloc(sizeof(struct airlinesCellule));
       if (tmp == NULL) {
	                   return ;
                        }
       tmp->liste.iata_code =cellule->iata_code;
       tmp->liste.airlineN =cellule->airlineN ;
       tmp->suivant=*list_air;
       *list_air =tmp;*/
       ajout_tete_airline(list_air, cellule);
       b++;
     }
	 
  return b;
}

void afficher_Airlines(struct airlinesCellule *airlineListe){
  while (airlineListe != NULL)
   {
   printf ("%s ,%s\n", airlineListe->liste.iata_code,airlineListe->liste.airlineN);
   airlineListe = airlineListe->suivant;
   }
}
/******************************charger le fichier "airports.csv"******************************/
void ajout_tete_airports(struct airportsCellule **list_airports, struct  airports cellule)
{
  struct airportsCellule *tmp = malloc(sizeof(struct airportsCellule));
       if (tmp == NULL)
	 {
	   return ;
         }
      
       tmp->liste =cellule;       
       tmp->suivant=*list_airports;
       *list_airports =tmp;

}

int fichier_airports(struct airportsCellule **list_airports,FILE *fairports)
 {
  char latitude[30]={'0'};
  char longitude[30]={'0'};
  if (fairports ==NULL){
    exit (EXIT_FAILURE); ;
  }
  int a=0;
  struct airports cellule;

  if(fscanf(fairports,"%*[^\n]\n")!=0) exit (EXIT_FAILURE); 

  
  while(fscanf(fairports, "%3s,",cellule.iata_code) ==1 )
     {
      if(fscanf(fairports,"%[^,]s,",cellule.airport) != 1)exit (EXIT_FAILURE);
      if(fscanf(fairports,",%[^,]s,",cellule.city) != 1)exit (EXIT_FAILURE);
      if(fscanf(fairports,",%[^,]s,",cellule.state) != 1)exit (EXIT_FAILURE);
      if(fscanf(fairports,",%[^,]s,",cellule.country) != 1)exit (EXIT_FAILURE);
      if( fscanf(fairports,",%[^,]s,",latitude) !=1)
	{
	  latitude[0] = '0';
	  latitude[1] ='\0';
	}
       cellule.latitude = atof(latitude);
       if(fscanf(fairports,",%[^\n]s\n",longitude) !=1)
	 {longitude[0] ='0';
	  longitude[1]='\n';
	 }
       cellule.longitude = atof(longitude);
       ajout_tete_airports(list_airports, cellule);
       a++;
     }
	 
  return a;
 }
void afficher_Airport (struct airportsCellule *airportListe)
{
    if (airportListe == NULL)
    {
        printf ("Le fichier Airport est vide\n");
        return;
    }
    while (airportListe != NULL)
    {
        printf ("%s,", airportListe->liste.iata_code);
        printf ("%s,", airportListe->liste.airport);
        printf ("%s,", airportListe->liste.city);
        printf ("%s,", airportListe->liste.state);
        printf ("%s,", airportListe->liste.country);
        printf ("%f,", airportListe->liste.latitude);
        printf ("%f\n", airportListe->liste.longitude);
        airportListe = airportListe->suivant;
    }
    }
/******************************fonction table hashage******************************/
void init_table_hash( ptr_Hashtab_flight hashtab)
{
  int i=0;
  while (i<SIZE){
    hashtab[i]=NULL;
    i++;
  }
}

int function_hash_date(int  month ,int day)
{   int index = 0;
    if (day >= 10)
        index = month * 100 + day;
    else
        index = month * 10 + day;

    return index % SIZE;
}

int function_hash_airline (char iata_airline[])
{
int j = 0;
int i = 0;

while (iata_airline[i] != '\0'){
    j += iata_airline[i];
    if (i == 1) j *= iata_airline[i];
    if (i == 0) j += iata_airline[i];
    i++;
    }

    return j % SIZE;
}
int function_hash_dest_and_dep (char iata_airport[])
{
int j = 0;
int i = 0;

while (iata_airport[i] != '\0')
    {
    j += iata_airport[i];
    if (i == 0) j += iata_airport[i];
    if (i == 1) j *= iata_airport[i];
    if (i == 2) j += iata_airport[i];
    i++;
    }
    return j % SIZE;
}

int nb_flights (struct flightCellule *list_flight ,int type)
{
  int cpt=0;

 while (list_flight != NULL)
    {
      cpt++;
      switch(type)
	{
	case DATE:
	  list_flight =list_flight->suivant_date;
	  break;
       	case AIRLINE:
	  list_flight =list_flight->suivant_airline;
	  break;
	case DEST_AIRPORT:
	  list_flight =list_flight->suivant_dest_airport;
	  break;
	case DEP_AIRPORT:
	  list_flight =list_flight->suivant_dep_airport;
	  break;
	}
    }
  return cpt;
}
/******************************charger le fichier "flights.csv"******************************/

void flight_init_cell(struct flightCellule **ptr_flight)
{
  if(*ptr_flight == NULL )return;
    (*ptr_flight)->suivant_date = NULL;
    (*ptr_flight)->suivant_airline = NULL;
    (*ptr_flight)->suivant_dest_airport = NULL;
    (*ptr_flight)->suivant_dep_airport  = NULL;

}

//adding_head_flight_read 
void adding_head_flight_read (struct flightCellule **ptr_flights_list_date,struct flightCellule **ptr_flights_list_airline,struct flightCellule **ptr_flights_list_dest_airport,struct flightCellule **ptr_flights_list_dep_airport,struct flight cellule)
{
    struct flightCellule *tmp = malloc (sizeof (struct flightCellule));
    flight_init_cell (&tmp);


    tmp->liste=cellule;
    tmp->suivant_date = *ptr_flights_list_date;
    tmp->suivant_airline = *ptr_flights_list_airline;
    tmp->suivant_dest_airport = *ptr_flights_list_dest_airport;
    tmp->suivant_dep_airport  = *ptr_flights_list_dep_airport;

    *ptr_flights_list_date = tmp;
    *ptr_flights_list_airline = tmp;
    *ptr_flights_list_dest_airport = tmp;
    *ptr_flights_list_dep_airport  = tmp;
}



int fichier_flights ( ptr_Hashtab_flight hashtab_date, ptr_Hashtab_flight hashtab_airline, ptr_Hashtab_flight hashtab_dest, ptr_Hashtab_flight hashtab_dep, FILE  *fflight)
{
    if (fflight == NULL) return 0;
    int cpt = 0;
    init_table_hash (hashtab_date);
    init_table_hash (hashtab_airline);
    init_table_hash (hashtab_dest);
    init_table_hash (hashtab_dep);
    struct flight f;

    if (fscanf (fflight, "%*[^\n]\n") != 0) exit (EXIT_FAILURE); 
    char temp[50] = { '\0' };
    while (fscanf (fflight, "%d,", &f.month) == 1)
    {
        if (fscanf (fflight, "%d,", &(f.day)) != 1) exit (EXIT_FAILURE);
        if (fscanf (fflight, "%d,", &(f.weekday)) != 1) exit (EXIT_FAILURE);
        if (fscanf (fflight, "%2s,", f.airline) != 1) exit (EXIT_FAILURE);
        if (fscanf (fflight, "%3s,", f.org_air) != 1) exit (EXIT_FAILURE);
        if (fscanf (fflight, "%3s,", f.dest_air) != 1) exit (EXIT_FAILURE);
        if (fscanf (fflight, "%d,", &(f.sched_dep)) != 1) exit (EXIT_FAILURE);
        if (fscanf (fflight, "%[^,]s,", temp) != 1)
        {
            temp[0] = '0';
            temp[1] = '\0';
        }
        f.dep_delay = atof (temp);
        if (fscanf (fflight, ",%[^,]s,", temp) != 1)
        {
            temp[0] = '0';
            temp[1] = '\0';
        }
        f.air_time = atof (temp);
        if (fscanf (fflight, ",%d,", &f.dist) != 1) exit (EXIT_FAILURE);
        if (fscanf (fflight, "%d,", &f.sched_arr) != 1) exit (EXIT_FAILURE);
        if (fscanf (fflight, "%[^,]s,", temp) != 1)
        {
            temp[0] = '0';
            temp[1] = '\0';
        }
        f.arr_delay = atof (temp);
        if (fscanf (fflight, ",%hhd,", &(f.diverted)) != 1) exit (EXIT_FAILURE);
        if (fscanf (fflight, "%hhd\n", &(f.cancelled)) != 1) exit (EXIT_FAILURE);

adding_head_flight_read (&(hashtab_date[function_hash_date (f.month, f.day)]),&(hashtab_airline[function_hash_airline (f.airline)]),&(hashtab_dest[function_hash_dest_and_dep (f.dest_air)]),&(hashtab_dep[function_hash_dest_and_dep (f.org_air)]), f);
        cpt++;
    }
    return cpt;
}



void print_flights (struct flightCellule *flightliste,int type)
{
    if (flightliste == NULL)
    {
    printf ("Le fichier Flight est vide!\n");
    return;
    }

    while (flightliste != NULL)
    {
    printf ("%d,", flightliste->liste.month);
    printf ("%d,", flightliste->liste.day);
    printf ("%d,", flightliste->liste.weekday);
    printf ("%s,", flightliste->liste.airline);
    printf ("%s,", flightliste->liste.org_air);
    printf ("%s,", flightliste->liste.dest_air);
    printf ("%d,", flightliste->liste.sched_dep);
    printf ("%f,", flightliste->liste.dep_delay);
    printf ("%f,", flightliste->liste.air_time);
    printf ("%d,", flightliste->liste.dist);
    printf ("%d,", flightliste->liste.sched_arr);
    printf ("%d,", flightliste->liste.arr_delay);
    printf ("%d,", flightliste->liste.diverted);
    printf ("%d\n", flightliste->liste.cancelled);

    switch(type)
     {
    case DATE:
     flightliste =flightliste->suivant_date;
     break;
    case AIRLINE:
     flightliste =flightliste->suivant_airline;
     break;
    case DEST_AIRPORT:
     flightliste =flightliste->suivant_dest_airport;
     break;
    case DEP_AIRPORT:
     flightliste =flightliste->suivant_dep_airport;
     break;
     }
    }
}

/******************************fonction necesere pour les commandes******************************/


  

void ajout_tete_flights(struct flightCellule **ptr_flight ,struct flight elem ,int type)
{
  struct flightCellule *newcell =malloc (sizeof (struct flightCellule ));
  flight_init_cell(&newcell);
  

  if(newcell == NULL)
    return;
  newcell->liste=elem;
  switch(type)
	{
	case DATE:
	   newcell->suivant_date =*ptr_flight;
	  break;
       	case AIRLINE:
	  newcell->suivant_airline =*ptr_flight;
	  break;
	case DEST_AIRPORT:
	  newcell->suivant_dest_airport =*ptr_flight;
	  break;
	case DEP_AIRPORT:
	  newcell->suivant_dep_airport =*ptr_flight;
	  break;
	}
  *ptr_flight=newcell;
}
void supp_flights_list(struct flightCellule **ptr_flight ,int type )
{
  if(*ptr_flight ==NULL)return;
  struct flightCellule *tmp =*ptr_flight;
  switch(type)
	{
	case DATE:
	  while((*ptr_flight)!=NULL)
	    {
	   tmp =*ptr_flight;
	   *ptr_flight =((*ptr_flight)->suivant_date);
	   free(tmp);
	    }
	  break;
       	case AIRLINE:
	  while((*ptr_flight)!=NULL)
	  {
	   tmp =*ptr_flight;
	   *ptr_flight =((*ptr_flight)->suivant_airline);
	   free(tmp);
	  }
	  break;
	case DEST_AIRPORT:
	  while((*ptr_flight)!=NULL)
	    {
	   tmp =*ptr_flight;
	   *ptr_flight =((*ptr_flight)->suivant_dest_airport);
	   free(tmp);
	    }
	  break;
	case DEP_AIRPORT:
	  while((*ptr_flight)!=NULL)
	    {
	  tmp =*ptr_flight;
	  *ptr_flight =((*ptr_flight)->suivant_dep_airport);
	  free(tmp);
	    }
	  break;
	}
}

void supp_airlines_list(struct airlinesCellule **ptr_airlines )
{
  if(*ptr_airlines==NULL)return;
  struct airlinesCellule *tmp =*ptr_airlines;
  while((*ptr_airlines)!=NULL)
	    {
	   tmp =*ptr_airlines;
	   *ptr_airlines =((*ptr_airlines)->suivant);
	   free(tmp);
	    }
}
void supp_airports_list(struct airportsCellule **ptr_airports )
{
   if(*ptr_airports==NULL)return;
  struct airportsCellule *tmp =*ptr_airports;
   while((*ptr_airports)!=NULL)
	    {
	   tmp =*ptr_airports;
	   *ptr_airports =((*ptr_airports)->suivant);
	   free(tmp);
	    }
}
void supp_data( ptr_Hashtab_flight hashtab_date, ptr_Hashtab_flight hashtab_airline, ptr_Hashtab_flight hashtab_dest, ptr_Hashtab_flight hashtab_dep,struct airlinesCellule** ptr_airlines,struct airportsCellule** ptr_airports)
{
  int i=0;
  while(i<SIZE)
  {
  struct flightCellule *tmp=hashtab_airline[i];
  supp_flights_list(&tmp,AIRLINE);
  i++;
  }
  init_table_hash(hashtab_date);
  init_table_hash(hashtab_airline);
  init_table_hash(hashtab_dest);
  init_table_hash(hashtab_dep);

  supp_airlines_list(ptr_airlines);
  supp_airports_list(ptr_airports);
}
	       
int ajoute_airports_sansduplication(struct airportsCellule **ptr_airports,struct airports tmp1)
{
  struct airportsCellule *newcell =malloc (sizeof (struct airportsCellule ));
  struct airportsCellule *tmp = *ptr_airports;
  if (tmp == NULL)
    {
      ajout_tete_airports(ptr_airports,tmp1);
      free(newcell);
      return 0;
    }
  int cpt =0;
  while(tmp !=NULL)
    {
      cpt++;
      tmp=tmp->suivant;
    }
  tmp=*ptr_airports;
  while(strcmp(tmp->liste.iata_code,tmp1.iata_code)!=0 && tmp->suivant !=NULL )
    {
      tmp=tmp->suivant;
    }
  if(strcmp(tmp->liste.iata_code,tmp1.iata_code)==0)
    {
      free(newcell);
      return cpt;
    }
  newcell->liste=tmp1;
  newcell->suivant=NULL;
  tmp->suivant=newcell;
  return cpt;
}
void ajoute_ordonner (struct flightCellule **ptr_flights, struct flight tmp1)
{
      if (*ptr_flights== NULL)
    {
      ajout_tete_flights (ptr_flights, tmp1, 2);
      return;
    }

  
    int max = 0;
    struct flightCellule *tmp = *ptr_flights;
    while (tmp != NULL)
    {
      max++;
      tmp = tmp->suivant_airline;
    }
    tmp = *ptr_flights;
    if (max <=4)
    {
       if (tmp->liste.arr_delay > tmp1.arr_delay)
       {
          ajout_tete_flights (ptr_flights, tmp1, 2);
          return;
       }
       while (tmp->suivant_airline != NULL && (tmp->suivant_airline->liste.arr_delay < tmp1.arr_delay))
        {
           tmp = tmp->suivant_airline;
        }

        struct flightCellule *new = malloc (sizeof (struct flightCellule));
        flight_init_cell (&new);
        new->liste = tmp1;
        if (tmp->suivant_airline == NULL && tmp->liste.arr_delay < tmp1.arr_delay)
        {
            new->suivant_airline = NULL;
            tmp->suivant_airline = new;
            return;
        }
        new->suivant_airline = tmp->suivant_airline;
        tmp->suivant_airline = new;
        return;
    }
    else
    {
        if (tmp->liste.arr_delay > tmp1.arr_delay) return;
        while (tmp->suivant_airline != NULL && (tmp->suivant_airline->liste.arr_delay < tmp1.arr_delay))
        {
            tmp = tmp->suivant_airline;
        }
        struct flightCellule *new = malloc (sizeof (struct flightCellule));
        flight_init_cell (&new);
        new->liste = tmp1;
        if (tmp->suivant_airline == NULL) 
        {
          new->suivant_airline = NULL;
          tmp->suivant_airline = new;
        }
        else
        {
          new->suivant_airline = tmp->suivant_airline;
          tmp->suivant_airline = new;
        }
        struct flightCellule *erase = *ptr_flights;
        *ptr_flights= (*ptr_flights)->suivant_airline;
        free (erase);
        return;
    }
}
/******************************fonction pour la commande show_airports*****************************/

void show_airports(char iata_airline[], ptr_Hashtab_flight hashtab_airline,struct airportsCellule *ap1,int nb_airports)
{
  struct airportsCellule *dep_air =NULL;
  struct flightCellule *fli =hashtab_airline[function_hash_airline(iata_airline)];
  struct airportsCellule *tmp =ap1;

  int cpt =0;

  while (fli != NULL)
    {
      while (tmp != NULL)
	{
	  if( strcmp(tmp->liste.iata_code,fli->liste.dest_air)==0 || strcmp(tmp->liste.iata_code,fli->liste.org_air) ==0)
	    {
	      cpt=ajoute_airports_sansduplication(&(dep_air),tmp->liste);
	      if (cpt == nb_airports) break;
	    }
	  tmp=tmp->suivant;
	}
      if (cpt == nb_airports) break;
      tmp=ap1;
      fli=fli->suivant_airline;
    }
  afficher_Airport (dep_air);
  supp_airports_list(&dep_air);
}

int ajoute_airline_sansduplication (struct airlinesCellule **airline_list_finale, struct airlines tmp1)
{
    if (airline_list_finale == NULL)
    {
      ajout_tete_airline(airline_list_finale, tmp1);
      return 1;
    }
    int cpt = 0;
    struct airlinesCellule *tmp = *airline_list_finale;
    while (tmp != NULL)
    {
      cpt++;
      tmp = tmp->suivant;
    }
    tmp = *airline_list_finale;
    while ( strcmp (tmp->liste.iata_code, tmp1.iata_code) != 0  && tmp->suivant != NULL)
    {
      tmp = tmp->suivant;
    }
    if (strcmp ( tmp1.iata_code,tmp->liste.iata_code) == 0) return cpt;
    struct airlinesCellule *newcell = malloc (sizeof (struct airlinesCellule));
    newcell->liste= tmp1;
    newcell->suivant= tmp->suivant;
    tmp->suivant= newcell;
    return cpt;
}

/******************************fonction pour la commande show_airlines******************************/

void show_airlines (char dep1[], ptr_Hashtab_flight hashtab_dep, struct airlinesCellule *airline_list, int nb_airlines)
{
    struct flightCellule * tmp = hashtab_dep[function_hash_dest_and_dep (dep1)];
    struct airlinesCellule *resu = NULL;
    struct airlinesCellule * recherche = NULL;
    int  cpt= 0;
    if (tmp == NULL || airline_list == NULL)
    {
        printf ("\n Aucun vol ou compagnie aérienne trouvé!\n");
        return;
    }
    while (tmp != NULL)
    {
       if (nb_airlines == cpt) break;
       recherche = airline_list;
       while (recherche != NULL)
        {
          if (strcmp ( recherche->liste.iata_code, tmp->liste.airline) == 0)
          {
             cpt = ajoute_airline_sansduplication(&resu, recherche->liste);
          }
          recherche = recherche->suivant;
        }
       tmp = tmp->suivant_dep_airport;
    }
    afficher_Airlines (resu);
    supp_airlines_list (&resu);
}

/******************************fonction pour la commande show_flights******************************/
void show_flights (char dep1[], int day, int month, ptr_Hashtab_flight hashtab_date, int time, int limite)
{
    struct flightCellule *flight_list = hashtab_date[function_hash_date (month, day)];
    int cpt = 0;
    struct flightCellule *resu = NULL;


        if (limite > 0 && time >= 0)
    {
      while (flight_list != NULL && cpt < limite)
      {
        if (strcmp (flight_list->liste.org_air, dep1) == 0 && flight_list->liste.sched_dep > time)
         {
           ajout_tete_flights (&resu, flight_list->liste, DATE);
           cpt++;
         }
        flight_list = flight_list->suivant_date;
      }
    }
    if (time >= 0 &&limite <= 0)
    {
      while (flight_list != NULL)
      {
        if (strcmp (flight_list->liste.org_air, dep1) == 0 && flight_list->liste.sched_dep > time)
         {
           ajout_tete_flights (&resu, flight_list->liste, DATE);
         }
           flight_list = flight_list->suivant_date;
      }
    }
    
    if (limite <= 0 && time < 0)
    {
      while (flight_list != NULL)
      {
        if (strcmp (flight_list->liste.org_air, dep1) == 0)
         {
           ajout_tete_flights (&resu, flight_list->liste, DATE);
         }
          flight_list = flight_list->suivant_date;
      }
    }
    if (limite > 0 && time < 0)
    {
      while (flight_list != NULL && cpt < limite)
      {
        if (strcmp (flight_list->liste.org_air, dep1) == 0)
         {
           ajout_tete_flights (&resu, flight_list->liste, DATE);
           cpt++;
         }
           flight_list = flight_list->suivant_date;
      }
    }
   
    print_flights (resu, DATE);
    supp_flights_list (&resu, DATE);
}



/******************************fonction pour la commande most_delayed_flight******************************/
void most_delayed_flight( ptr_Hashtab_flight hashtab_airline)
{
  struct flightCellule *final = NULL;
  struct flightCellule *temp =NULL;
  int i=0;
 while ( i<SIZE ) {

     temp = hashtab_airline[i];
     while(temp!=NULL){
     ajoute_ordonner(&final,temp->liste);
     temp=temp->suivant_airline;
     i++;
    }
    i++;
  }
  print_flights (final, 2);;
  supp_flights_list (&final, 2);
}

/******************************fonction pour la commande necesaire pour la commande delayed_airline******************************/
int delayed_airline (char iata_airline[], ptr_Hashtab_flight hashtab_airline, struct airlinesCellule *all1, int print)
{
  int retard = 0;
  char airline_nom[50];
  int nb_flights = 0;
  struct flightCellule *fli = hashtab_airline[function_hash_airline (iata_airline)];

  while (fli != NULL)
   {
        if (print == 1)
    {
     while (all1 != NULL)
      {
	if (strcmp ( all1->liste.iata_code ,iata_airline) == 0)
        {
          strcpy (airline_nom, all1->liste.airlineN);
        }
        all1 = all1->suivant;
      }
      printf ("\n%s,%s,%d\n", iata_airline, airline_nom, retard/ nb_flights);
    }
	if (strcmp ( fli->liste.airline , iata_airline) == 0)
    {
     retard =retard + fli->liste.arr_delay;
     nb_flights++;
    }
     fli = fli->suivant_airline;
   }

int moy=retard/nb_flights;
    return moy;
}
 
/******************************fonction pour la commande most_delayed_airlines******************************/

void most_delayed_airlines (ptr_Hashtab_flight hash_airline, struct airlinesCellule *liste , int nb_airlines)
{
  struct airlinesCellule *final = NULL;
  struct airline_delay tab[nb_airlines];
  struct airline_delay temp;
  int i=0;
  int j=0;

  while (liste!=NULL)
    {
     tab[i].airline = liste->liste;
     tab[i].delay = delayed_airline(liste->liste.iata_code, hash_airline, liste, NON);
     liste = liste->suivant;
     i++;
    }
  for (int a=1 ; a<=nb_airlines ; a++)
    { 
      for (int b=0 ; b < nb_airlines-a ; b++)
    {
      if (tab[b].delay < tab[b+1].delay)
        {
          temp=tab[b+1];
          tab[b+1] =tab[b];
          tab[b]=temp;
        }
    }
    }

  for (j=0 ; j<=4; j++)
    {
     ajout_tete_airline (&final, tab[j].airline);
    }
  printf("\n");
  afficher_Airlines(final);
  printf("\n");
  supp_airlines_list(&final);
  return;
}
/******************************fonction pour la commande avg_flight_duration******************************/
void changed_flights (unsigned char month, unsigned char day,  ptr_Hashtab_flight hashtab_date)
{
struct flightCellule *tmp = hashtab_date[function_hash_date (month, day)];
struct flightCellule *resu = NULL;
   while (tmp != NULL)
    {
    if ( ( tmp->liste.cancelled == 1 || tmp->liste.diverted == 1) && tmp->liste.day == day && tmp->liste.month == month)          {
      ajout_tete_flights(&resu, tmp->liste, 1);
      }
    tmp = tmp->suivant_date;
    }
    printf ("\n");
    print_flights(resu, 1);
    printf ("\n");
    supp_flights_list (&resu, 1);
}
void avg_flight_duration( char id_org[], char id_dest[] , ptr_Hashtab_flight hashtab_airport)
{
  float avg_airtime = 0;
  int cpt =0;
  struct flightCellule *tmp = hashtab_airport[function_hash_dest_and_dep (id_org)];
  if(tmp==NULL)
    {
      printf("aucun vol trouvé\n");
      return;
    }

  while(tmp!=NULL)
    {
    if (strcmp(tmp->liste.org_air,id_org)==0 && strcmp(tmp->liste.dest_air, id_dest )==0)
    {
      avg_airtime=avg_airtime + tmp->liste.air_time;
      cpt++;
    }
      tmp=tmp->suivant_dep_airport;
    }
  printf("\n average : %.1f minutes (%d flight)\n", avg_airtime / cpt , cpt);
}

/******************************fonction main******************************/
int main() {
  char suite[200];
  char nom[40];
  char requete[150];
  char *tmp;
  int x;
  const char * space = " ";

printf("\n----------------Bonjour----------------\n");
printf("\nBienvenue dans notre application\n");
printf("\nCe Projet est realiser par Omar SIFA et Iyass MEHARZI\n");
  
  printf("Download files .......\n");
  printf("\n");
  FILE *fairlines = fopen("data/airlines.csv","r");
  if (fairlines==NULL)
    {printf("erreure on peux pas lire le fichier");
      return 0;
    }
  printf("fichier airlines.csv charger \n");

   FILE *fairports = fopen("data/airports.csv","r");
  if (fairports==NULL)
    {printf("erreure");
      return 0;
    }
  printf("fichier airports.csv charger \n");

    FILE *fflight = fopen("data/flights.csv","r");
  if (fflight==NULL)
    {printf("erreure");
      return 0;
    }
  printf("fichier flights.csv charger \n");

  FILE * fp= fopen ("data/requete.txt","r");
 if (fp==NULL)
    {printf("erreure");
      return 0;
    }
   printf("fichier requete.txt charger \n");
   printf("\n");
   printf("Download successful \n");
   printf("\n");
  struct airlinesCellule *airlinesC=NULL;
  struct airportsCellule *airportsC=NULL ;

    ptr_Hashtab_flight hashtab_date;
    ptr_Hashtab_flight hashtab_airline;
    ptr_Hashtab_flight hashtab_dest;
    ptr_Hashtab_flight hashtab_dep;

  int nb_airlines= fichier_airlines(&airlinesC,fairlines);
  //afficher_Airlines(airlinesC);
  printf("il y a %d airlines\n",nb_airlines);
 
  int nb_airports= fichier_airports(&airportsC,fairports);
  printf("il y a %d airports\n",nb_airports);
  // afficher_Airport(airportsC);
  
  int nb_flights=fichier_flights(hashtab_date, hashtab_airline,hashtab_dest,hashtab_dep,fflight);
  printf("il y a %d flights\n",nb_flights);

printf("\nSi vous voulez voir le test de nos requetes tapez 1 si vous voulez lire le fichier requete.txt tapez 2\n");
printf("votre choix est : ");
scanf("%d",&x);
if (x==1){

  //test commandes
  printf ("\n*********************Test requetes*********************\n");
  
  printf ("\nTest show-airports:\n");
  show_airports ("HA", hashtab_airline,airportsC , nb_airports);

  printf ("\nTest show-airlines:\n");
  // show_airlines ("LAX", hashtab_dep, airlinesC, nb_airlines);
  printf("On a pas reussi la commande show-airlines \n");
  
  printf ("\nTest show-flights:\n");
  show_flights ("LAX", 1, 3, hashtab_date, -5, 10);

  printf ("\nTest most-delayed_flights:\n");
  most_delayed_flight (hashtab_airline);

  printf ("\nTest most-delayed-airlines:\n");
  most_delayed_airlines (hashtab_airline,airlinesC, nb_airlines);
   
  printf ("\nTest changed-flights:\n");
  changed_flights (12, 29, hashtab_date);

  printf ("\nTest avg-flight-duration:\n");
  avg_flight_duration ("LAX", "SFO", hashtab_dep);
   printf ("\n*********************fin Test requetes*********************\n");
  
 }
if (x==2){

   printf ("\n*********************requetes de requete.txt*********************\n");
  while(fgets(requete,150,fp) != NULL){
    tmp = strtok(requete,space);
    
    if(tmp == NULL)
      {
      printf("rien n'est lu\n");
      }
    else
      {      
      strcpy(nom,tmp);
      }
    tmp = strtok(NULL," \n");
    
    if(tmp != NULL)
      {
      strcpy(suite,tmp);
      if(strcmp(nom,"show-airports") == 0)
	{
	 printf ("\nshow-airports:\n");
         show_airports (suite, hashtab_airline,airportsC , nb_airports);
	 printf("\n");
        }
      
      if(strcmp(nom,"show-airlines") == 0)
	{
	  printf ("\nshow-airlines:\n");
         // show_airlines (suite, hashtab_dep, airlinesC, nb_airlines);
	  printf("On a pas reussi la commande show-airlines \n");
        }

      if(strcmp(nom,"show-flights") == 0)
	{
        printf ("\nshow-flights:\n");

        show_flights (suite, 1, 3, hashtab_date, -5, 10);
        }

      if(strcmp(nom,"avg-flight-duration") == 0)
	{
        printf ("\navg-flight-duration:\n");
        avg_flight_duration ("LAX", "SFO", hashtab_dep);
        }
      
      if(strcmp(nom,"find-itinerary") == 0)
	{
	  printf ("\nfind-itinerary:\n");
	  printf("On a pas reussi la commande find-itinerary \n");
        }

      if(strcmp(nom,"most-delayed-flights") == 0)
	{
	printf ("\nmost-delayed-flights:\n");
        most_delayed_flight(hashtab_airline);
        }

      if(strcmp(nom,"most-delayed-airlines") == 0)
	{
	 printf ("\nmost-delayed-airlines:\n");
         most_delayed_airlines(hashtab_airline,airlinesC, nb_airlines);
        }
      
      if(strcmp(nom,"changed-flights") == 0)
	{
	  printf ("\nchanged-flights:\n");
          changed_flights (12, 29, hashtab_date);
        }
      if(strcmp(nom,"airlines") == 0)
	{
	  printf ("\nairlines:\n");
	  printf("On a pas reussi la commande airlines \n");
        }       
    }
    }
}
   fclose(fairlines);
  fclose(fairports);
  fclose(fflight);
  fclose(fp);
  
  return 0;
  
}
